package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import main.Animal.AnimalBuilder;

public class Zoo {

    public static final String ID = "ID";
    public static final String NAME = "NAME";
    public static final String TYPE = "TYPE";
    public static final String HABITAT = "HABITAT";
    public static final String ISLOANED = "ISLOANED";
    public static final String NUTRITIONS = "NUTRITIONS";

    private static final Path ANIMALS = Paths.get(System.getProperty("user.dir"), "animals.txt");
    private List<Animal> animals = new ArrayList<>();

    public Zoo() {
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public void addAnimal(Animal animal) {
        animals.add(animal);
        saveAnimals();
    }

    public Optional<Animal> getAnimalById(long id) {
        for (Animal animal : animals) {
            if (animal.getId() == id) {
                return Optional.of(animal);
            }
        }
        return Optional.empty();
    }

    public Optional<Animal> getAnimalByName(String name) {
        for (Animal animal : animals) {
            if (animal.getName().equals(name)) {
                return Optional.of(animal);
            }
        }
        return Optional.empty();
    }

    public void removeAnimal(Animal animal) {
        animals.remove(animal);
        saveAnimals();
    }

    public void updateAnimal(Animal animal) {
        for (Animal a : animals) {
            if (a.getId() == animal.getId()) {
                animals.set(animals.indexOf(a), animal);
            }
        }
        saveAnimals();
    }

    public void printNutritionsOfAnimal(Animal animal) {
        System.out.format("%-15s%-15s\n", ID, "NUTRITION");
        animal.getNutritions().forEach((k, v) -> System.out.format("%-15s%-15s\n", Long.toString(k), v));
    }

    public void listAnimals(Boolean order) {
        if (animals.isEmpty()) {
            System.out.format("%-15s%-15s%-15s%-15s%-15s\n", ID, NAME, TYPE, HABITAT, ISLOANED);
        } else {
            System.out.format(getAnimalTableFormat(), ID, NAME, TYPE, HABITAT, ISLOANED);
            if (order) {
                Collections.sort(animals, (o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
            } else {
                Collections.sort(animals, (o1, o2) -> o2.getName().compareToIgnoreCase(o1.getName()));
            }
            for (Animal animal : animals) {
                System.out.format(getAnimalTableFormat(), animal.printOut());
            }
        }
    }

    public void printAnimal(Animal animal) {
        System.out.format(getAnimalTableFormat(animal), ID, NAME, TYPE, HABITAT, ISLOANED);
        System.out.format(getAnimalTableFormat(animal), animal.printOut());
    }

    public void loadAnimals() {
        loadAnimals(ANIMALS);
    }

    public boolean loadAnimals(Path path) {
        animals.clear();
        if (Files.exists(path)) {
            File file = new File(path.toString());
            AnimalBuilder animalBuilder = Animal.AnimalBuilder.newInstance();
            BufferedReader br;
            try {
                br = new BufferedReader(new FileReader(file));
                String animal;
                while ((animal = br.readLine()) != null) {
                    String[] animalArray = animal.split(",");
                    String nutritionsString = animal.substring(animal.indexOf("{")).replace("{", "").replace("}", "");
                    List<String> nutritionsList = Arrays.asList(nutritionsString.split(";"));
                    Map<Long, String> nutritions = nutritionsList.stream()
                            .collect(Collectors.toMap(n -> Long.valueOf(n.split("=")[0]), n -> n.split("=")[1]));
                    animalBuilder.withId(Long.valueOf(animalArray[0])).withName(animalArray[1]).withType(animalArray[2])
                            .withHabitat(animalArray[3]).withIsLoaned(Boolean.valueOf(animalArray[4]))
                            .withNutritions(nutritions);
                    animals.add(animalBuilder.build());
                }
                br.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            return false;
        }
        return false;
    }

    public void saveAnimals() {
        saveAnimals(ANIMALS);
    }

    public void saveAnimals(Path path) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path.toFile()));
            for (Animal animal : animals) {
                writer.write(
                        String.join(",", Arrays.asList(animal.printOut())) + ",{" + animal.printNutritions() + "}\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getAnimalTableFormat(Animal animal) {
        return getAnimalTableFormat(Arrays.asList(animal));
    }

    private String getAnimalTableFormat() {
        return getAnimalTableFormat(animals);
    }

    private String getAnimalTableFormat(List<Animal> animals) {
        String format = "%-15s";

        int largestString = animals.get(0).getName().length();
        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i).getName().length() > largestString) {
                largestString = animals.get(i).getName().length();
            }
        }
        format = format.concat("%-" + (largestString + 2) + "s");

        largestString = animals.get(0).getType().length();
        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i).getType().length() > largestString) {
                largestString = animals.get(i).getType().length();
            }
        }
        format = format.concat("%-" + (largestString + 2) + "s");

        largestString = animals.get(0).getHabitat().length();
        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i).getHabitat().length() > largestString) {
                largestString = animals.get(i).getHabitat().length();
            }
        }
        format = format.concat("%-" + (largestString + 2) + "s" + "%-" + (largestString + 2) + "s");
        format = format.concat("\n");

        return format;
    }
}
