package main;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Animal {

    private String name;
    private String type;
    private String habitat;
    private long id;
    private Boolean isLoaned;
    private Map<Long, String> nutritions = new HashMap<>();

    private Animal() {
    }

    public static class AnimalBuilder {

        private String name;
        private String type;
        private String habitat;
        private long id;
        private Boolean isLoaned;
        private Map<Long, String> nutritions = new HashMap<>();

        public AnimalBuilder() {
            this.id = System.currentTimeMillis();
        }

        public static AnimalBuilder newInstance() {
            return new AnimalBuilder();
        }

        public AnimalBuilder withId(long id) {
            this.id = id;
            return this;
        }

        public AnimalBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public AnimalBuilder withType(String type) {
            this.type = type;
            return this;
        }

        public AnimalBuilder withHabitat(String habitat) {
            this.habitat = habitat;
            return this;
        }

        public AnimalBuilder withIsLoaned(Boolean isLoaned) {
            this.isLoaned = isLoaned;
            return this;
        }

        public AnimalBuilder withNutritions(Map<Long, String> nutritions) {
            this.nutritions = nutritions;
            return this;
        }

        public Animal build() {
            Animal animal = new Animal();
            animal.id = this.id;
            animal.name = this.name;
            animal.type = this.type;
            animal.habitat = this.habitat;
            animal.isLoaned = this.isLoaned;
            animal.nutritions = this.nutritions;
            return animal;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getIsLoaned() {
        return isLoaned;
    }

    public void setIsLoaned(Boolean isLoaned) {
        this.isLoaned = isLoaned;
    }

    public Map<Long, String> getNutritions() {
        return nutritions;
    }

    public void setNutritions(Map<Long, String> nutritions) {
        this.nutritions = nutritions;
    }

    public void addNutrition(String nutrition) {
        this.nutritions.put(System.currentTimeMillis(), nutrition);
    }

    public String[] printOut() {
        return new String[] { Long.toString(this.id), this.name, this.type, this.habitat, this.isLoaned.toString() };
    }

    public String printNutritions() {
        return nutritions.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue())
                .collect(Collectors.joining(";"));
    }

}
