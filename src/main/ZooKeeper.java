package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.IntStream;

import main.Animal.AnimalBuilder;

public class ZooKeeper {

    private static final String WRONG_COMMAND_PLEASE_TRY_AGAIN = "Wrong command, please try again!";
    private static final String ERROR_HAPPENED_DURING_ACTION_READING = "Error happened during action reading: ";
    private static final String HELP_FORMAT = "%-15s%-15s\n";
    private static final String COULD_NOT_FIND_ANIMAL = "Could not find animal in database!";
    private static final String PLEASE_ENTER = "Please enter ";
    private static final String PLEASE_ENTER_A = PLEASE_ENTER + "a ";
    private static final String PLEASE_ENTER_ID = PLEASE_ENTER + "the ID of the animal you would like to ";
    private static final String PLEASE_ENTER_WHAT_TO_DO = PLEASE_ENTER + "what would you like to do";
    private static Zoo zoo;

    public static void initZoo() {
        zoo = new Zoo();
        zoo.loadAnimals();
    }

    public static void main(String[] args) {
        initZoo();
        System.out.println("Welcome to our Zookepper application");
        System.out.println(PLEASE_ENTER_WHAT_TO_DO + " (Enter \"help\" for help)");

        Boolean terminated = false;
        while (!terminated) {
            String action;
            Optional<Animal> optionalAnimal = Optional.empty();
            Boolean loanValue = false;
            Boolean nameValue = false;
            Animal animal;
            String id;
            Boolean subTerminated;
            String path;
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter command: ");
                action = br.readLine();

                String[] actionParts = action.split(" ");
                if (actionParts[0].equals("nutrition") && actionParts.length > 2) {
                    String subAction = actionParts[2].equals("add") && actionParts.length >= 4 ? "add"
                            : actionParts[2].equals("list") && actionParts.length == 3 ? "list"
                                    : actionParts[2].equals("remove") && actionParts.length >= 4 ? "remove" : "";
                    String identifier = actionParts[1];
                    if (isNumeric(identifier)) {
                        optionalAnimal = zoo.getAnimalById(Long.valueOf(identifier));
                    } else {
                        optionalAnimal = zoo.getAnimalByName(identifier);
                    }
                    if (optionalAnimal.isPresent()) {
                        animal = optionalAnimal.get();

                        switch (subAction) {
                        case "add":
                            String nutrition = String.join(" ", IntStream.range(3, actionParts.length)
                                    .mapToObj(i -> actionParts[i]).toArray(String[]::new));
                            animal.addNutrition(nutrition);
                            zoo.updateAnimal(animal);
                            System.out.println("Nutrition added to animal!");
                            action = "handled";
                            break;
                        case "list":
                            zoo.printAnimal(animal);
                            System.out.println();
                            zoo.printNutritionsOfAnimal(animal);
                            action = "handled";
                            break;
                        case "remove":
                            if (isNumeric(actionParts[3])
                                    && animal.getNutritions().containsKey(Long.valueOf(actionParts[3]))) {
                                animal.getNutritions().remove(Long.valueOf(actionParts[3]));
                                zoo.updateAnimal(animal);
                                System.out.println("Nutrition removed from animal!");
                            } else {
                                System.err.println("Could not find nutrition with given ID to this animal!");
                            }
                            action = "handled";
                            break;
                        default:
                            System.err.println(WRONG_COMMAND_PLEASE_TRY_AGAIN);
                            action = "handled";
                            break;
                        }
                    } else {
                        System.err.println(COULD_NOT_FIND_ANIMAL);
                        action = "handled";
                    }
                }

                switch (action) {
                case "export":
                    path = readUntilNotEmpty(PLEASE_ENTER_A + "file path to export to: ");
                    zoo.saveAnimals(Paths.get(path));
                    System.out.println("Animal database exported successfully!");
                    break;
                case "import":
                    path = readUntilNotEmpty(PLEASE_ENTER_A + "file path to import from: ");
                    if (zoo.loadAnimals(Paths.get(path))) {
                        System.out.println("Animal database imported successfully!");
                    } else {
                        System.err.println("Could not find file!");
                    }
                    break;
                case "help":
                    printCommands();
                    break;
                case "list":
                    zoo.listAnimals(Boolean.TRUE);
                    break;
                case "list desc":
                    zoo.listAnimals(Boolean.FALSE);
                    break;
                case "add":
                    AnimalBuilder animalBuilder = Animal.AnimalBuilder.newInstance();
                    while (!nameValue) {
                        final String name = readUntilNotEmpty(PLEASE_ENTER_A + "name: ");
                        Boolean isUnique = zoo.getAnimals().stream().anyMatch(a -> a.getName().equals(name));
                        if (isUnique) {
                            System.err.println("Animal name must be unique");
                            System.err.println();
                        } else {
                            nameValue = true;
                            animalBuilder.withName(name);
                        }
                    }
                    System.out.println();
                    animalBuilder.withType(readUntilNotEmpty(PLEASE_ENTER_A + "type: "));
                    System.out.println();
                    animalBuilder.withHabitat(readUntilNotEmpty(PLEASE_ENTER_A + "habitat: "));
                    System.out.println();
                    String isLoaned = "";
                    while (!loanValue) {
                        System.out.print("Is this animal loaned to another zoo? (true/false): ");
                        isLoaned = br.readLine();
                        if (isLoaned.equals(Boolean.TRUE.toString()) || isLoaned.equals(Boolean.FALSE.toString())) {
                            loanValue = true;
                        } else {
                            System.err.println("Value can only be true or false!");
                        }
                    }
                    animalBuilder.withIsLoaned(Boolean.valueOf(isLoaned));
                    System.out.println();
                    zoo.addAnimal(animalBuilder.build());
                    System.out.println("Animal added to zoo!");
                    break;
                case "remove":
                    id = readUntilNotEmpty(PLEASE_ENTER_ID + "remove: ");
                    if (isNumeric(id)) {
                        optionalAnimal = zoo.getAnimalById(Long.valueOf(id));
                    }
                    if (optionalAnimal.isPresent()) {
                        zoo.removeAnimal(optionalAnimal.get());
                        System.out.println("Animal removed from zoo!");
                    } else {
                        System.err.println(COULD_NOT_FIND_ANIMAL);
                    }
                    break;
                case "loan":
                    id = readUntilNotEmpty(PLEASE_ENTER_ID + "manage: ");
                    if (isNumeric(id)) {
                        optionalAnimal = zoo.getAnimalById(Long.valueOf(id));
                    }
                    if (optionalAnimal.isPresent()) {
                        animal = optionalAnimal.get();
                        zoo.printAnimal(animal);
                    } else {
                        System.err.println(COULD_NOT_FIND_ANIMAL);
                        break;
                    }
                    System.out.println(PLEASE_ENTER_WHAT_TO_DO + ":");
                    String subAction = br.readLine();
                    subTerminated = false;
                    while (!subTerminated) {
                        switch (subAction) {
                        case "start":
                            if (animal.getIsLoaned()) {
                                System.err.println("Animal already loaned to another zoo!");
                            } else {
                                animal.setIsLoaned(Boolean.TRUE);
                                zoo.updateAnimal(animal);
                                System.out.println("Animal loaned to another zoo!");
                            }
                            subTerminated = true;
                            break;
                        case "end":
                            if (!animal.getIsLoaned()) {
                                System.err.println("Animal is not loaned to another zoo!");
                            } else {
                                animal.setIsLoaned(Boolean.FALSE);
                                zoo.updateAnimal(animal);
                                System.out.println("Animal returned from another zoo!");
                            }
                            subTerminated = true;
                            break;
                        case "cancel":
                            subTerminated = true;
                            break;
                        default:
                            System.err.println("Wrong subcommand, please try again!");
                            break;
                        }
                    }
                    break;
                case "handled":
                    break;
                case "exit":
                    zoo.saveAnimals();
                    System.out.println("Please press ENTER and rest in peace...");
                    br.readLine();
                    terminated = true;
                    break;
                default:
                    System.err.println(WRONG_COMMAND_PLEASE_TRY_AGAIN);
                    break;
                }
            } catch (IOException e) {
                System.err.println(ERROR_HAPPENED_DURING_ACTION_READING + e);
            }
        }
        System.exit(0);

    }

    public static boolean isNumeric(String id) {
        return id.matches("-?\\d+?");
    }

    public static String readUntilNotEmpty(String printout) {
        Boolean isNotEmpty = false;
        String value = "";
        while (!isNotEmpty) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print(printout);
                value = br.readLine();
                if (value.isEmpty()) {
                    System.err.println("Value cannot be empty!");
                } else {
                    isNotEmpty = Boolean.TRUE;
                }
            } catch (IOException e) {
                System.err.println(ERROR_HAPPENED_DURING_ACTION_READING + e);
            }
        }
        return value;
    }

    private static void printCommands() {
        System.out.println("ZooKeeper 1.0");
        System.out.println("Available commands: ");
        System.out.format(HELP_FORMAT, "list", "List all animals in the zoo in ascending order");
        System.out.format(HELP_FORMAT, "list desc", "List all animals in the zoo in descending order");
        System.out.format(HELP_FORMAT, "add", "Add new animal to the zoo");
        System.out.format(HELP_FORMAT, "remove", "Remove an animal from the zoo based on it's ID");
        System.out.format(HELP_FORMAT, "loan", "Loan/reposess an animal to/from another zoo based on it's ID");
        System.out.format("%-5s%-10s%-15s\n", "", "start", "Loan an animal to another zoo");
        System.out.format("%-5s%-10s%-15s\n", "", "end", "Reposess an animal from another zoo");
        System.out.format("%-5s%-10s%-15s\n", "", "cancel", "Back to main menu");
        System.out.format(HELP_FORMAT, "nutrition <animal name/ID> add <nutrition text>",
                "Add special nutrition, additional care to animal");
        System.out.format(HELP_FORMAT, "nutrition <animal name/ID> list",
                "List special nutritions, additional care of animal");
        System.out.format(HELP_FORMAT, "nutrition <animal name/ID> remove <nutrition ID>",
                "Remove special nutrition, additional care of animal");
        System.out.format(HELP_FORMAT, "exit", "Exit from the application");
    }

}
